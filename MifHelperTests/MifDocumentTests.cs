﻿using FluentAssertions;
using MifHelper.Document;
using MifHelper.Parser;
using MifHelper.Tags;
using System.IO;
using System.Linq;
using Xunit;

namespace MifHelperTests
{
    public class MifDocumentTests
    {
        private readonly MifDocument mifDocument;

        public MifDocumentTests()
        {
            var tagResolver = new TagResolver();
            TagParser.SpecificTagResolverFunction = tagResolver.GetTag;

            mifDocument = new();
            mifDocument.Childs.Add(new MIFFileTag(2019));
            mifDocument.Childs.Add(new ParaTag());
            mifDocument.FindMifNodes<ParaTag>().First().Childs.Add(new ParaLineTag());
            StringTag stringTag = new("Hello World");
            mifDocument.FindMifNodes<ParaLineTag>().First().Childs.Add(stringTag);
            mifDocument.InsertAfter(stringTag, new MifComment("My first MIF file."));
        }

        [Fact]
        public void ConvertMifDocumentToTokensTest()
        {
            var convertResultA = mifDocument.AsTokens().ToList();
            var convertResultB = mifDocument.AsTokens().ToList();
            convertResultA.Should().BeEquivalentTo(convertResultB);
        }

        [Fact]
        public void ConvertMifDocumentToString()
        {
            var convertResultA = mifDocument.ConvertToString();
            var convertResultB = mifDocument.ConvertToString();
            convertResultA.Should().BeEquivalentTo(convertResultB);
        }

        [Fact]
        public void DocumentReadWriteTest()
        {
            string mifContent = mifDocument.ConvertToString();
            MifDocument newMifDocument = new(mifContent);
            string newMifContent = newMifDocument.ConvertToString();
            mifContent.Should().Be(newMifContent);
        }

        [Fact]
        public void DeepCloneTest()
        {
            string originalMifContent = mifDocument.ConvertToString();

            MifDocument clonedMifDocument = mifDocument.DeepClone();
            string clonedOriginalContent = clonedMifDocument.ConvertToString();
            clonedOriginalContent.Should().Be(originalMifContent);

            StringTag clonedStringTag = clonedMifDocument.FindMifNodes<StringTag>().Single();
            clonedStringTag.Value = "Changed!";

            string originalMifContent2 = mifDocument.ConvertToString();
            string clonedMifContent2 = clonedMifDocument.ConvertToString();

            clonedMifContent2.Should().NotBe(originalMifContent2);
        }

        [Fact]
        public void MifTestFileReadWriteTest()
        {
            string testMifDocumentString = File.ReadAllText("Test.mif");
            MifDocument testMifDocument = new(testMifDocumentString);
            var resultString = testMifDocument.ConvertToString();
            testMifDocumentString = testMifDocumentString
                .Replace(" ", "")
                .Replace("\t", "")
                .Replace("\n", "")
                .Replace("\r", "");
            resultString = resultString
                .Replace(" ", "")
                .Replace("\t", "")
                .Replace("\n", "")
                .Replace("\r", "");
            resultString.Should().Be(testMifDocumentString);
        }

    }
}
