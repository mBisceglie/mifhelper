﻿using MifHelper.Parser;
using MifHelper.Tokenizer;
using System;
using System.Collections.Generic;
using System.Linq;

namespace MifHelper.Document
{
    /// <summary>
    /// MIF document class for handling MIF files and efficient operations to modify them.
    /// </summary>
    public class MifDocument : IMifNodeWithChilds
    {
        private readonly Dictionary<IMifNode, IMifNodeWithChilds> parentDict = new();

        public List<IMifNode> Childs { get; }

        /// <summary>
        /// Creates a empty MIF document.
        /// </summary>
        public MifDocument() { Childs = new(); }

        /// <summary>
        /// Creates a MIF document from a string containing the content of a MIF document.
        /// </summary>
        public MifDocument(string mifDocument)
        {
            var tokens = MifTokenizer.Tokenize(mifDocument);
            Childs = MifParser.Parse(tokens).ToList();
        }

        public MifDocument(IList<MifToken> tokens)
        {
            Childs = MifParser.Parse(tokens).ToList();
        }

        /// <summary>
        /// Creates a MIF document from a list of nodes representing the content of the MIF document.
        /// </summary>
        public MifDocument(List<IMifNode> mifNodes)
        {
            Childs = mifNodes;
        }

        public override string ToString() => base.ToString() ?? nameof(MifDocument);

        /// <summary>
        /// Converts the MIF document in a stream of tokens.
        /// </summary>
        public IEnumerable<MifToken> AsTokens(int level = 0) => Childs.SelectMany(x => x.AsTokens(level));

        /// <summary>
        /// Converts the MIF document into the string representation of the MIF document.
        /// The string e.g. can be saved as a MIF file.
        /// </summary>
        /// <returns>The string representing the MIF file.</returns>
        public string ConvertToString() => MifTokenizer.ConvertTokensToString(AsTokens());

        /// <summary>
        /// Creates a deep clone of the MifDocument.
        /// </summary>
        public MifDocument DeepClone() => new(AsTokens().ToList());

        /// <summary>
        /// Searches the parent of a node in the MIF document.
        /// For high performance access a dictionary is accessed internally which is reinitialized if the parent is not found in it.
        /// Accesses to nodes that are not in the dictionary therefore always result in a full scan of all nodes for the rebuilding of the dictionary. 
        /// </summary>
        public IMifNodeWithChilds? FindParent(IMifNode mifNode)
        {
            if (parentDict.TryGetValue(mifNode, out var mifNodeParent))
            {
                var child = mifNode;
                var parent = mifNodeParent;
                while (parent != null)
                {
                    if (parent == this && Childs.Contains(child)) { break; }
                    if (parent.Childs.Contains(child))
                    {
                        child = parent;
                        parentDict.TryGetValue(child, out parent);
                    }
                    else
                    {
                        parent = null;
                        break;
                    }
                }
                if (parent != null) { return mifNodeParent; }
            }
            BuildParentDict();
            parentDict.TryGetValue(mifNode, out mifNodeParent);
            return mifNodeParent;
        }

        /// <summary>
        /// Checks if a node is part of the MIF document.
        /// </summary>
        public bool IsPartOfDocument(IMifNode mifNode)
        {
            return FindParent(mifNode) != null;
        }

        /// <summary>
        /// Replaces one node in the MIF document with another.
        /// </summary>
        public bool Replace(IMifNode oldMifNode, IMifNode newMifNode)
        {
            return InsertAfter(oldMifNode, newMifNode) && Remove(oldMifNode);
        }

        /// <summary>
        /// Replaces one node in the MIF document with collection of nodes.
        /// </summary>
        public bool Replace(IMifNode oldMifNode, IEnumerable<IMifNode> newMifNodes)
        {
            return InsertAfter(oldMifNode, newMifNodes) && Remove(oldMifNode);
        }

        /// <summary>
        /// Removes a node and all its childs from the MIF document.
        /// </summary>
        public bool Remove(IMifNode mifNode)
        {
            var parent = FindParent(mifNode);
            if (parent is IMifNodeWithChilds mifNodeWithChilds)
            {
                return mifNodeWithChilds.Childs.Remove(mifNode);
            }
            return false;
        }

        /// <summary>
        /// Inserts a new node before the one specified in the MIF document.
        /// </summary>
        public bool InsertBefore(IMifNode mifNode, IMifNode newMifNode) => InsertBefore(mifNode, newMifNodes: newMifNode);

        /// <summary>
        /// Inserts new nodes before the one specified in the MIF document.
        /// </summary>
        public bool InsertBefore(IMifNode mifNode, params IMifNode[] newMifNodes) => InsertBefore(mifNode, (IEnumerable<IMifNode>)newMifNodes);

        /// <summary>
        /// Inserts new nodes before the one specified in the MIF document.
        /// </summary>
        public bool InsertBefore(IMifNode mifNode, IEnumerable<IMifNode> newMifNodes)
        {
            var parent = FindParent(mifNode);
            if (parent is IMifNodeWithChilds mifNodeWithChilds)
            {
                var index = mifNodeWithChilds.Childs.IndexOf(mifNode);
                if (index != -1)
                {
                    foreach (var node in newMifNodes.Reverse())
                    {
                        mifNodeWithChilds.Childs.Insert(index, node);
                    }
                    return true;
                }
            }
            return false;
        }

        /// <summary>
        /// Inserts a new node after the one specified in the MIF document.
        /// </summary>
        public bool InsertAfter(IMifNode mifNode, IMifNode newMifNode) => InsertAfter(mifNode, newMifNodes: newMifNode);

        /// <summary>
        /// Inserts new nodes after the one specified in the MIF document.
        /// </summary>
        public bool InsertAfter(IMifNode mifNode, params IMifNode[] newMifNodes) => InsertAfter(mifNode, (IEnumerable<IMifNode>)newMifNodes);

        /// <summary>
        /// Inserts new nodes after the one specified in the MIF document.
        /// </summary>
        public bool InsertAfter(IMifNode mifNode, IEnumerable<IMifNode> newMifNodes)
        {
            var parent = FindParent(mifNode);
            if (parent is IMifNodeWithChilds mifNodeWithChilds)
            {
                var index = mifNodeWithChilds.Childs.IndexOf(mifNode);
                if (index != -1)
                {
                    foreach (var node in newMifNodes.Reverse())
                    {
                        mifNodeWithChilds.Childs.Insert(index + 1, node);
                    }
                    return true;
                }
            }
            return false;
        }

        private void BuildParentDict(IMifNodeWithChilds? parent = null, IEnumerable<IMifNode>? mifNodes = null)
        {
            if (parent is null)
            {
                parent = this;
                parentDict.Clear();
            }
            if (mifNodes is null) { mifNodes = Childs; }
            foreach (var node in mifNodes)
            {
                parentDict[node] = parent;
                if (node is IMifNodeWithChilds nodeWithChilds && nodeWithChilds.Childs.Count > 0)
                {
                    BuildParentDict(nodeWithChilds, nodeWithChilds.Childs);
                }
            }
        }

    }
}
