﻿using MifHelper.Parser;
using System.Collections.Generic;

namespace MifHelper.Document
{
    public static class TagExtensions
    {
        public static void AddChilds(this IMifNodeWithChilds obj, params IMifNode?[] childs)
        {
            AddChilds(obj, (IEnumerable<IMifNode?>)childs);
        }

        public static void AddChilds(this IMifNodeWithChilds obj, IEnumerable<IMifNode?> childs)
        {
            foreach (IMifNode? child in childs)
            {
                if (child is null) { continue; }
                obj.Childs.Add(child);
            }
        }
    }
}
