﻿namespace MifHelper.Enums
{
    public enum StartPageSides
    {
        ReadFromFile,
        NextAvailableSide,
        StartLeftSide,
        StartRightSide
    }
}
