﻿namespace MifHelper.Parser
{
    /// <summary>
    /// Base interface for all tags in the MIF.
    /// </summary>
    public interface IMifTag : IMifNode
    {
        /// <summary>
        /// The name of the tag.
        /// </summary>
        public string TagName { get; }

        /// <summary>
        /// The optional end tag comment.
        /// </summary>
        public MifComment? TagEndComment { get; set; }
    }

}
