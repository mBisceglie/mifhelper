﻿using MifHelper.Tokenizer;
using System.Collections.Generic;
using System.Linq;

namespace MifHelper.Parser
{
    /// <summary>
    /// Base class for all tags in the MIF.
    /// </summary>
    public class MifTag : IMifTagWithChilds
    {
        public MifTag(string tagName, List<MifToken> parameters, List<IMifNode> childs)
        {
            TagName = tagName;
            Parameters = parameters;
            Childs = childs;
        }

        public string TagName { get; }
        public List<MifToken> Parameters { get; }
        public List<IMifNode> Childs { get; }
        public MifComment? TagEndComment { get; set; }

        public override string ToString() => $"<{TagName} {string.Join(' ', Parameters.Select(x => x.Value))} {(Childs.Any() ? "..." : "")}>";

        public IEnumerable<MifToken> AsTokens(int level = 0)
        {
            foreach (var token in this.GetTagStartAsTokens(level)) { yield return token; };
            foreach (var token in Parameters)
            {
                yield return MifTokenFactory.WhiteSpaceToken;
                yield return token;
                if (token.Type == MifTokenType.Comment) { yield return MifTokenFactory.LineBreakToken; }
            }
            foreach (var token in this.GetChildsAsTokens(level)) { yield return token; };
            foreach (var token in this.GetTagEndAsTokens(level)) { yield return token; };
        }

    }
}
