﻿using MifHelper.Tokenizer;
using System.Collections.Generic;
using System.Linq;

namespace MifHelper.Parser
{
    /// <summary>
    /// Utility class with extension methods to convert tags to tokens more easily.
    /// </summary>
    public static class TagExtensions
    {
        /// <summary>
        /// Creates the tokens for the start part of the tag.
        /// </summary>
        public static IEnumerable<MifToken> GetTagStartAsTokens(this IMifTag mifTag, int level)
        {
            if (level > 0) { yield return MifTokenFactory.FromIndent(level); }
            yield return MifTokenFactory.TagStartToken;
            yield return MifTokenFactory.FromTagName(mifTag.TagName);
        }

        /// <summary>
        /// Creates the tokens for the child section of the tag.
        /// </summary>
        public static IEnumerable<MifToken> GetChildsAsTokens(this IMifTagWithChilds mifTag, int level)
        {
            if (mifTag.Childs.Count > 0)
            {
                IEnumerable<IMifNode> childs = mifTag.Childs;
                if (mifTag.Childs[0] is MifComment)
                {
                    foreach (var token in mifTag.Childs[0].AsTokens(1)) { yield return token; }
                    childs = childs.Skip(1);
                }
                else
                {
                    yield return MifTokenFactory.LineBreakToken;
                }
                foreach (var child in childs)
                {
                    foreach (var token in child.AsTokens(level + 1)) { yield return token; }
                }
                if (level > 0) { yield return MifTokenFactory.FromIndent(level); }
            }
        }

        /// <summary>
        /// Creates the tokens for the end part of the tag.
        /// </summary>
        public static IEnumerable<MifToken> GetTagEndAsTokens(this IMifTag mifTag, int level)
        {
            yield return MifTokenFactory.TagEndToken;
            if (mifTag.TagEndComment != null)
            {
                yield return MifTokenFactory.WhiteSpaceToken;
                foreach (var token in mifTag.TagEndComment.AsTokens()) { yield return token; }
                yield break;
            }
            yield return MifTokenFactory.LineBreakToken;
        }

    }
}
