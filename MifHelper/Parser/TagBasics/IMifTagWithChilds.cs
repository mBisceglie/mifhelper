﻿namespace MifHelper.Parser
{
    /// <summary>
    /// Base interface for all tags that can have childs.
    /// </summary>
    public interface IMifTagWithChilds : IMifTag, IMifNodeWithChilds
    {
    }

}
