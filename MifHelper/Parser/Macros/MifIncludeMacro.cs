﻿using MifHelper.Tokenizer;
using System.Collections.Generic;

namespace MifHelper.Parser
{
    /// <summary>
    /// Inlcude macro statement.
    /// </summary>
    public class MifIncludeMacro : IMifMacro
    {
        public MifIncludeMacro(string path) => Path = path;

        public string Path { get; set; }

        public override string ToString() => $"include ({Path})";

        public IEnumerable<MifToken> AsTokens(int level = 0)
        {
            if (level > 0) { yield return MifTokenFactory.FromIndent(level); }
            yield return new MifToken(MifTokenType.Identifier, "include");
            yield return MifTokenFactory.BracketStartToken;
            yield return new MifToken(MifTokenType.Path, Path);
            yield return MifTokenFactory.BracketEndToken;
            yield return MifTokenFactory.LineBreakToken;
        }

    }
}
