﻿namespace MifHelper.Parser
{
    /// <summary>
    /// Base interface for all macro statements in the MIF format.
    /// </summary>
    public interface IMifMacro : IMifNode
    {
    }
}
