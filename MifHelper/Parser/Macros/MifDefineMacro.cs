﻿using MifHelper.Tokenizer;
using System.Collections.Generic;
using System.Linq;

namespace MifHelper.Parser
{
    /// <summary>
    /// Define macro statement.
    /// </summary>
    public class MifDefineMacro : IMifMacro
    {
        public MifDefineMacro(string name, List<MifToken> replacement)
        {
            Name = name;
            Replacement = replacement;
        }

        public string Name { get; set; }

        public List<MifToken> Replacement { get; set; }

        public override string ToString() => $"define ({Name}, {string.Join(' ', Replacement.Select(x => x.Value))})";

        public IEnumerable<MifToken> AsTokens(int level = 0)
        {
            if (level > 0) { yield return MifTokenFactory.FromIndent(level); }
            yield return new MifToken(MifTokenType.Identifier, "define");
            yield return MifTokenFactory.BracketStartToken;
            yield return new MifToken(MifTokenType.Identifier, Name);
            yield return MifTokenFactory.CommaToken;
            foreach (var token in Replacement) { yield return token; }
            yield return MifTokenFactory.BracketEndToken;
            yield return MifTokenFactory.LineBreakToken;
        }

    }
}