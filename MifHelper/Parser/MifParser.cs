﻿using MifHelper.Tokenizer;
using System;
using System.Collections.Generic;

namespace MifHelper.Parser
{
    /// <summary>
    /// Parser for the MIF format.
    /// </summary>
    public static class MifParser
    {
        public static IEnumerable<IMifNode> Parse(IList<MifToken> tokens)
        {
            for (int pos = 0; pos < tokens.Count; pos++)
            {
                var token = tokens[pos];
                switch (token.Type)
                {
                    case MifTokenType.WhiteSpace:
                    case MifTokenType.LineBreak:
                        break;
                    case MifTokenType.Comment:
                        yield return new MifComment(token.Value);
                        break;
                    case MifTokenType.Identifier:
                        yield return MacroParser.ParseMacro(tokens, ref pos);
                        break;
                    case MifTokenType.TagStart:
                        yield return TagParser.ParseTag(tokens, ref pos);
                        break;
                    default:
                        throw new ArgumentException($"Syntax error (at pos {pos}): Invalid token!");
                }
            }
        }

    }
}
