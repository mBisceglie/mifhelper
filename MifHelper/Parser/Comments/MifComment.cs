﻿using MifHelper.Tokenizer;
using System.Collections.Generic;

namespace MifHelper.Parser
{
    /// <summary>
    /// The MifComment class respresents a comment in the MIF format.
    /// </summary>
    public class MifComment : IMifNode
    {
        public MifComment(string comment) => Comment = comment;

        public string Comment { get; set; }

        public override string ToString() => Comment;

        public IEnumerable<MifToken> AsTokens(int level = 0)
        {
            if (level > 0) { yield return MifTokenFactory.FromIndent(level); }
            yield return MifTokenFactory.FromComment(Comment);
            yield return MifTokenFactory.LineBreakToken;
        }

    }
}
