﻿using MifHelper.Tokenizer;
using System.Collections.Generic;

namespace MifHelper.Parser
{
    /// <summary>
    /// Base interface for all elements in the MIF.
    /// </summary>
    public interface IMifNode
    {
        /// <summary>
        /// Converts the node to tokens.
        /// </summary>
        public IEnumerable<MifToken> AsTokens(int level = 0);

        public string ToString();
    }
}
