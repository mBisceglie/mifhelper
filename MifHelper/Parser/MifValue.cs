﻿using MifHelper.Tokenizer;
using System;
using System.Collections.Generic;

namespace MifHelper.Parser
{
    /// <summary>
    /// Base class for all values in the MIF.
    /// </summary>
    public class MifValue : IMifNode
    {
        public MifValue(string value) => Value = value;

        /// <summary>
        /// The value.
        /// </summary>
        public string Value { get; set; }

        /// <summary>
        /// The optional unit.
        /// </summary>
        public string? Unit { get; set; }

        public override string ToString() => $"{Value}{(Unit is null ? "" : Unit)}>";

        public IEnumerable<MifToken> AsTokens(int value = 0)
        {
            throw new NotImplementedException();
        }

    }
}
