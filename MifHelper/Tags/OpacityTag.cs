﻿using MifHelper.Common;
using MifHelper.Parser;
using MifHelper.Tokenizer;
using System.Collections.Generic;
using System.Globalization;

namespace MifHelper.Tags
{
    public class OpacityTag : IMifTag
    {
        public string TagName => "Opacity";
        public MifComment? TagEndComment { get; set; }
        public int Opacity { get; set; }

        public OpacityTag(int opacity) => Opacity = opacity;

        public OpacityTag(List<MifToken> parameters, List<IMifNode> childs)
        {
            this.ValidateParameters(parameters, MifTokenType.Number);
            this.ValidateChilds(childs, false);
            Opacity = int.Parse(parameters[0].Value, CultureInfo.InvariantCulture);
        }

        public override string ToString() => $"<{TagName} {Opacity}>";

        public IEnumerable<MifToken> AsTokens(int level)
        {
            foreach (var token in this.GetTagStartAsTokens(level)) { yield return token; };
            yield return MifTokenFactory.WhiteSpaceToken;
            yield return MifTokenFactory.FromNumber(Opacity);
            foreach (var token in this.GetTagEndAsTokens(level)) { yield return token; };
        }
    }

}
