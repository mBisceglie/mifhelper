﻿using MifHelper.Common;
using MifHelper.Parser;
using MifHelper.Tokenizer;
using System;
using System.Collections.Generic;
using System.Globalization;

namespace MifHelper.Tags
{
    public class MIFFileTag : IMifTag
    {
        public string TagName => "MIFFile";
        public MifComment? TagEndComment { get; set; }
        public decimal Version { get; set; }

        public MIFFileTag(decimal version) => Version = version;

        public MIFFileTag(List<MifToken> parameters, List<IMifNode> childs)
        {
            this.ValidateParameters(parameters, MifTokenType.None);
            this.ValidateChilds(childs, false);
            if (parameters[0].Type != MifTokenType.DecimalNumber && parameters[0].Type != MifTokenType.Number)
            {
                throw new Exception($"The '{TagName}' tag supports only a number ot decimal number as parameter!");
            }
            Version = decimal.Parse(parameters[0].Value, CultureInfo.InvariantCulture);
        }

        public override string ToString() => $"<{TagName} {Version.ToString(CultureInfo.InvariantCulture)}>";

        public IEnumerable<MifToken> AsTokens(int level)
        {
            foreach (var token in this.GetTagStartAsTokens(level)) { yield return token; };
            yield return MifTokenFactory.WhiteSpaceToken;
            yield return MifTokenFactory.FromDecimalNumber(Version);
            foreach (var token in this.GetTagEndAsTokens(level)) { yield return token; };
        }
    }
}
