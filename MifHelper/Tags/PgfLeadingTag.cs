﻿using MifHelper.Common;
using MifHelper.Enums;
using MifHelper.Parser;
using MifHelper.Tokenizer;
using System.Collections.Generic;
using System.Globalization;

namespace MifHelper.Tags
{
    public class PgfLeadingTag : IMifTag
    {
        public string TagName => "PgfLeading";
        public MifComment? TagEndComment { get; set; }

        public decimal Space { get; set; }
        public MifUnit Unit { get; set; }

        public PgfLeadingTag(List<MifToken> parameters, List<IMifNode> childs)
        {
            this.ValidateParameters(parameters, MifTokenType.DecimalNumber, MifTokenType.Identifier);
            this.ValidateChilds(childs, false);
            Space = decimal.Parse(parameters[0].Value, CultureInfo.InvariantCulture);
            Unit = MifUnitHelper.StringToMifUnit(parameters[1].Value);
        }

        public PgfLeadingTag(string value, MifUnit unit) : this(decimal.Parse(value, CultureInfo.InvariantCulture), unit) { }

        public PgfLeadingTag(decimal value, MifUnit unit)
        {
            Space = value;
            Unit = unit;
        }

        public override string ToString() => $"<{TagName} {Space}{Unit}>";

        public IEnumerable<MifToken> AsTokens(int level)
        {
            foreach (var token in this.GetTagStartAsTokens(level)) { yield return token; };
            yield return MifTokenFactory.WhiteSpaceToken;
            yield return MifTokenFactory.FromDecimalNumber(Space);
            yield return MifTokenFactory.FromIdentifier(MifUnitHelper.MifUnitToString(Unit));
            foreach (var token in this.GetTagEndAsTokens(level)) { yield return token; };
        }
    }
}
