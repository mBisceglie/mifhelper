﻿using MifHelper.Parser;
using MifHelper.Tokenizer;
using System.Collections.Generic;

namespace MifHelper.Tags
{
    /// <summary>
    /// Tag resolver for converting tags to concrete MIF tag implementations instead of the generic MifTag when parsing. 
    /// </summary>
    public class TagResolver
    {
        /// <summary>
        /// Helper method that creates a matching implementation for the specified tag name.
        /// </summary>
        /// <param name="tagName">The name of the tag to create.</param>
        /// <param name="parameters">The tokens that represent the parameters of the tag.</param>
        /// <param name="childs">The child nodes.</param>
        /// <returns>The specific tag instance.</returns>
        public IMifTag GetTag(string tagName, List<MifToken> parameters, List<IMifNode> childs)
        {
            return tagName switch
            {
                "MIFFile" => new MIFFileTag(parameters, childs),
                "ParaLine" => new ParaLineTag(parameters, childs),
                "Para" => new ParaTag(parameters, childs),
                "String" => new StringTag(parameters, childs),
                "ImportObFileDI" => new ImportObFileDITag(parameters, childs),
                "ID" => new IDTag(parameters, childs),
                "GroupID" => new GroupIDTag(parameters, childs),
                "Unique" => new UniqueTag(parameters, childs),
                "CharTag" => new CharTag(parameters, childs),
                "ImportObject" => new ImportObjectTag(parameters, childs),
                "ImportHint" => new ImportHintTag(parameters, childs),
                "ImportObFile" => new ImportObFileTag(parameters, childs),
                "TblBody" => new TblBodyTag(parameters, childs),
                "FileName" => new FileNameTag(parameters, childs),
                "ShapeRect" => new ShapeRectTag(parameters, childs),
                "BRect" => new BRectTag(parameters, childs),
                "ObColor" => new ObColorTag(parameters, childs),
                "Point" => new PointTag(parameters, childs),
                "PgfTag" => new PgfTagTag(parameters, childs),
                "TextRectID" => new TextRectIDTag(parameters, childs),
                "CellColor" => new CellColorTag(parameters, childs),
                "TblID" => new TblIDTag(parameters, childs),
                "RowWithPrev" => new RowWithPrevTag(parameters, childs),
                "RowWithNext" => new RowWithNextTag(parameters, childs),
                "Tbls" => new TblsTag(parameters, childs),
                "Tbl" => new TblTag(parameters, childs),
                "TblH" => new TblHTag(parameters, childs),
                "ATbl" => new ATblTag(parameters, childs),
                "TblTag" => new TblTagTag(parameters, childs),
                "TblFormat" => new TblFormatTag(parameters, childs),
                "TblSpBefore" => new TblSpBeforeTag(parameters, childs),
                "TblSpAfter" => new TblSpAfterTag(parameters, childs),
                "TblCellMargins" => new TblCellMarginsTag(parameters, childs),
                "TblTitlePlacement" => new TblTitlePlacementTag(parameters, childs),
                "TblNumColumns" => new TblNumColumnsTag(parameters, childs),
                "TblColumnWidth" => new TblColumnWidthTag(parameters, childs),
                "PgfSpAfter" => new PgfSpAfterTag(parameters, childs),
                "PgfSpBefore" => new PgfSpBeforeTag(parameters, childs),
                "RowHeight" => new RowHeightTag(parameters, childs),
                "CellColumns" => new CellColumnsTag(parameters, childs),
                "CellRows" => new CellRowsTag(parameters, childs),
                "CellLRuling" => new CellLRulingTag(parameters, childs),
                "CellBRuling" => new CellBRulingTag(parameters, childs),
                "CellRRuling" => new CellRRulingTag(parameters, childs),
                "CellTRuling" => new CellTRulingTag(parameters, childs),
                "CellFill" => new CellFillTag(parameters, childs),
                "Font" => new FontTag(parameters, childs),
                "FStretch" => new FStretchTag(parameters, childs),
                "CellAngle" => new CellAngleTag(parameters, childs),
                "Pgf" => new PgfTag(parameters, childs),
                "PgfCellAlignment" => new PgfCellAlignmentTag(parameters, childs),
                "PgfLeading" => new PgfLeadingTag(parameters, childs),
                "Row" => new RowTag(parameters, childs),
                "PgfCellMargins" => new PgfCellMarginsTag(parameters, childs),
                "AFrame" => new AFrameTag(parameters, childs),
                "AFrames" => new AFramesTag(parameters, childs),
                "Fill" => new FillTag(parameters, childs),
                "Frame" => new FrameTag(parameters, childs),
                "Cell" => new CellTag(parameters, childs),
                "CellContent" => new CellContentTag(parameters, childs),
                "HyphenMaxLines" => new HyphenMaxLinesTag(parameters, childs),
                "PageNum" => new PageNumTag(parameters, childs),
                "TextFlow" => new TextFlowTag(parameters, childs),
                "TextRect" => new TextRectTag(parameters, childs),
                "FTag" => new FTagTag(parameters, childs),
                "Overprint" => new OverprintTag(parameters, childs),
                "PageType" => new PageTypeTag(parameters, childs),
                "PenWidth" => new PenWidthTag(parameters, childs),
                "Rectangle" => new RectangleTag(parameters, childs),
                "Pen" => new PenTag(parameters, childs),
                "Ellipse" => new EllipseTag(parameters, childs),
                "PolyLine" => new PolyLineTag(parameters, childs),
                "NumPoints" => new NumPointsTag(parameters, childs),
                "FPosition" => new FPositionTag(parameters, childs),
                "FLocked" => new FLockedTag(parameters, childs),
                "Angle" => new AngleTag(parameters, childs),
                "PgfAlignment" => new PgfAlignmentTag(parameters, childs),
                "PgfFont" => new PgfFontTag(parameters, childs),
                "FColor" => new FColorTag(parameters, childs),
                "StartPageSide" => new StartPageSideTag(parameters, childs),
                "Opacity" => new OpacityTag(parameters, childs),
                "RowPlacement" => new RowPlacementTag(parameters, childs),
                "PgfNumTabs" => new PgfNumTabsTag(parameters, childs),
                "TabStop" => new TabStopTag(parameters, childs),
                "TSX" => new TsxTag(parameters, childs),
                "TSType" => new TsTypeTag(parameters, childs),
                "TSLeaderStr" => new TsLeaderStrTag(parameters, childs),
                "Page" => new PageTag(parameters, childs),
                // TODO Create all other tags
                _ => new MifTag(tagName, parameters, childs)
            };
        }

    }
}
