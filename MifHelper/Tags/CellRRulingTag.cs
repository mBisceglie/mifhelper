﻿using MifHelper.Common;
using MifHelper.Parser;
using MifHelper.Tokenizer;
using System.Collections.Generic;

namespace MifHelper.Tags
{
    public class CellRRulingTag : IMifTag
    {
        public string TagName => "CellRRuling";
        public MifComment? TagEndComment { get; set; }
        public string Value { get; set; }

        public CellRRulingTag(string value) => Value = value;

        public CellRRulingTag(List<MifToken> parameters, List<IMifNode> childs)
        {
            this.ValidateParameters(parameters, MifTokenType.String);
            this.ValidateChilds(childs, false);
            Value = MifStringHelper.DecodeString(parameters[0].Value.TrimStart('`').TrimEnd('\''));
        }

        public override string ToString() => $"<{TagName} `{MifStringHelper.EncodeString(Value)}'>";

        public IEnumerable<MifToken> AsTokens(int level)
        {
            foreach (var token in this.GetTagStartAsTokens(level)) { yield return token; };
            yield return MifTokenFactory.WhiteSpaceToken;
            yield return MifTokenFactory.FromString(MifStringHelper.EncodeString(Value));
            foreach (var token in this.GetTagEndAsTokens(level)) { yield return token; };
        }
    }
}
