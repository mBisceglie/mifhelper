﻿using MifHelper.Common;
using MifHelper.Enums;
using MifHelper.Parser;
using MifHelper.Tokenizer;
using System.Collections.Generic;
using System.Globalization;

namespace MifHelper.Tags
{
    public class PgfCellMarginsTag : IMifTag
    {
        public string TagName => "PgfCellMargins";
        public MifComment? TagEndComment { get; set; }

        public decimal Left { get; set; }
        public decimal Top { get; set; }
        public decimal Right { get; set; }
        public decimal Bottom { get; set; }
        public MifUnit LeftUnit { get; set; }
        public MifUnit TopUnit { get; set; }
        public MifUnit RightUnit { get; set; }
        public MifUnit BottomUnit { get; set; }

        public PgfCellMarginsTag(List<MifToken> parameters, List<IMifNode> childs)
        {
            this.ValidateValueWithUnitParameters(parameters, 4);
            this.ValidateChilds(childs, false);
            Left = decimal.Parse(parameters[0].Value, CultureInfo.InvariantCulture);
            LeftUnit = MifUnitHelper.StringToMifUnit(parameters[1].Value);
            Top = decimal.Parse(parameters[2].Value, CultureInfo.InvariantCulture);
            TopUnit = MifUnitHelper.StringToMifUnit(parameters[3].Value);
            Right = decimal.Parse(parameters[4].Value, CultureInfo.InvariantCulture);
            RightUnit = MifUnitHelper.StringToMifUnit(parameters[5].Value);
            Bottom = decimal.Parse(parameters[6].Value, CultureInfo.InvariantCulture);
            BottomUnit = MifUnitHelper.StringToMifUnit(parameters[7].Value);
        }

        public PgfCellMarginsTag(string xPosition, string yPosition, string width, string height, MifUnit unit) :
            this(xPosition, yPosition, width, height, unit, CultureInfo.InvariantCulture)
        { }

        public PgfCellMarginsTag(string xPosition, string yPosition, string width, string height, MifUnit unit, CultureInfo c) :
            this(decimal.Parse(xPosition, c), decimal.Parse(yPosition, c), decimal.Parse(width, c), decimal.Parse(height, c), unit)
        { }

        public PgfCellMarginsTag(decimal xPosition, decimal yPosition, decimal width, decimal height, MifUnit unit)
        {
            Left = xPosition;
            LeftUnit = unit;
            Top = yPosition;
            TopUnit = unit;
            Right = width;
            RightUnit = unit;
            Bottom = height;
            BottomUnit = unit;
        }

        public override string ToString() => $"<{TagName} {Left} {LeftUnit} {Top} {TopUnit} {Right} {RightUnit} {Bottom} {BottomUnit}>";

        public IEnumerable<MifToken> AsTokens(int level)
        {
            foreach (var token in this.GetTagStartAsTokens(level)) { yield return token; };
            yield return MifTokenFactory.WhiteSpaceToken;
            yield return MifTokenFactory.FromNumber(Left);
            yield return MifTokenFactory.WhiteSpaceToken;
            yield return MifTokenFactory.FromIdentifier(MifUnitHelper.MifUnitToString(LeftUnit));
            yield return MifTokenFactory.WhiteSpaceToken;
            yield return MifTokenFactory.FromNumber(Top);
            yield return MifTokenFactory.WhiteSpaceToken;
            yield return MifTokenFactory.FromIdentifier(MifUnitHelper.MifUnitToString(TopUnit));
            yield return MifTokenFactory.WhiteSpaceToken;
            yield return MifTokenFactory.FromNumber(Right);
            yield return MifTokenFactory.WhiteSpaceToken;
            yield return MifTokenFactory.FromIdentifier(MifUnitHelper.MifUnitToString(RightUnit));
            yield return MifTokenFactory.WhiteSpaceToken;
            yield return MifTokenFactory.FromNumber(Bottom);
            yield return MifTokenFactory.WhiteSpaceToken;
            yield return MifTokenFactory.FromIdentifier(MifUnitHelper.MifUnitToString(BottomUnit));
            foreach (var token in this.GetTagEndAsTokens(level)) { yield return token; };
        }
    }
}
