﻿using MifHelper.Common;
using MifHelper.Parser;
using MifHelper.Tokenizer;
using System.Collections.Generic;
using System.Linq;

namespace MifHelper.Tags
{
    public class TblBodyTag : IMifTagWithChilds
    {
        public string TagName => "TblBody";
        public MifComment? TagEndComment { get; set; }
        public List<IMifNode> Childs { get; }

        public TblBodyTag() => Childs = new List<IMifNode>();

        public TblBodyTag(List<MifToken> parameters, List<IMifNode> childs)
        {
            this.ValidateParameters(parameters);
            this.ValidateChilds(childs, true);
            Childs = childs;
        }

        public override string ToString() => $"<{TagName} {(Childs.Any() ? "..." : "")}>";

        public IEnumerable<MifToken> AsTokens(int level)
        {
            foreach (var token in this.GetTagStartAsTokens(level)) { yield return token; };
            foreach (var token in this.GetChildsAsTokens(level)) { yield return token; };
            foreach (var token in this.GetTagEndAsTokens(level)) { yield return token; };
        }
    }
}
