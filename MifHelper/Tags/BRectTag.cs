﻿using MifHelper.Common;
using MifHelper.Enums;
using MifHelper.Parser;
using MifHelper.Tokenizer;
using System.Collections.Generic;
using System.Globalization;

namespace MifHelper.Tags
{
    // Till MIF V 3.0 use <ShapeRect> in MIF > V 4.0
    public class BRectTag : IMifTag
    {
        public string TagName => "BRect";
        public MifComment? TagEndComment { get; set; }

        public decimal Left { get; set; }
        public decimal Top { get; set; }
        public decimal Width { get; set; }
        public decimal Height { get; set; }
        public MifUnit LeftUnit { get; set; }
        public MifUnit TopUnit { get; set; }
        public MifUnit WidthUnit { get; set; }
        public MifUnit HeightUnit { get; set; }

        public BRectTag(List<MifToken> parameters, List<IMifNode> childs)
        {
            this.ValidateValueWithUnitParameters(parameters, 4);
            this.ValidateChilds(childs, false);

            Left = decimal.Parse(parameters[0].Value, CultureInfo.InvariantCulture);
            LeftUnit = MifUnitHelper.StringToMifUnit(parameters[1].Value);
            Top = decimal.Parse(parameters[2].Value, CultureInfo.InvariantCulture);
            TopUnit = MifUnitHelper.StringToMifUnit(parameters[3].Value);
            Width = decimal.Parse(parameters[4].Value, CultureInfo.InvariantCulture);
            WidthUnit = MifUnitHelper.StringToMifUnit(parameters[5].Value);
            Height = decimal.Parse(parameters[6].Value, CultureInfo.InvariantCulture);
            HeightUnit = MifUnitHelper.StringToMifUnit(parameters[7].Value);
        }

        public BRectTag(string left, string top, string width, string height, MifUnit unit) :
            this(left, top, width, height, unit, CultureInfo.InvariantCulture)
        { }

        public BRectTag(string left, string top, string width, string height, MifUnit unit, CultureInfo c) :
            this(decimal.Parse(left, c), decimal.Parse(top, c), decimal.Parse(width, c), decimal.Parse(height, c), unit)
        { }

        public BRectTag(decimal left, decimal top, decimal width, decimal height, MifUnit unit)
        {
            Left = left;
            LeftUnit = unit;
            Top = top;
            TopUnit = unit;
            Width = width;
            WidthUnit = unit;
            Height = height;
            HeightUnit = unit;
        }

        public override string ToString() => $"<{TagName} {Left} {LeftUnit} {Top} {TopUnit} {Width} {WidthUnit} {Height} {HeightUnit}>";

        public IEnumerable<MifToken> AsTokens(int level)
        {
            foreach (var token in this.GetTagStartAsTokens(level)) { yield return token; };
            yield return MifTokenFactory.WhiteSpaceToken;
            yield return MifTokenFactory.FromNumber(Left);
            yield return MifTokenFactory.WhiteSpaceToken;
            yield return MifTokenFactory.FromIdentifier(MifUnitHelper.MifUnitToString(LeftUnit));
            yield return MifTokenFactory.WhiteSpaceToken;
            yield return MifTokenFactory.FromNumber(Top);
            yield return MifTokenFactory.WhiteSpaceToken;
            yield return MifTokenFactory.FromIdentifier(MifUnitHelper.MifUnitToString(TopUnit));
            yield return MifTokenFactory.WhiteSpaceToken;
            yield return MifTokenFactory.FromNumber(Width);
            yield return MifTokenFactory.WhiteSpaceToken;
            yield return MifTokenFactory.FromIdentifier(MifUnitHelper.MifUnitToString(WidthUnit));
            yield return MifTokenFactory.WhiteSpaceToken;
            yield return MifTokenFactory.FromNumber(Height);
            yield return MifTokenFactory.WhiteSpaceToken;
            yield return MifTokenFactory.FromIdentifier(MifUnitHelper.MifUnitToString(HeightUnit));
            foreach (var token in this.GetTagEndAsTokens(level)) { yield return token; };
        }
    }
}
