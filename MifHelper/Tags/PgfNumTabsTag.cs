﻿using MifHelper.Common;
using MifHelper.Parser;
using MifHelper.Tokenizer;
using System.Collections.Generic;
using System.Globalization;

namespace MifHelper.Tags
{
    public class PgfNumTabsTag : IMifTag
    {
        public string TagName => "PgfNumTabs";
        public MifComment? TagEndComment { get; set; }
        public int Count { get; set; }

        public PgfNumTabsTag(int count) => Count = count;

        public PgfNumTabsTag(List<MifToken> parameters, List<IMifNode> childs)
        {
            this.ValidateParameters(parameters, MifTokenType.Number);
            this.ValidateChilds(childs, false);
            Count = int.Parse(parameters[0].Value, CultureInfo.InvariantCulture);
        }

        public override string ToString() => $"<{TagName} {Count}>";

        public IEnumerable<MifToken> AsTokens(int level)
        {
            foreach (var token in this.GetTagStartAsTokens(level)) { yield return token; };
            yield return MifTokenFactory.WhiteSpaceToken;
            yield return MifTokenFactory.FromNumber(Count);
            foreach (var token in this.GetTagEndAsTokens(level)) { yield return token; };
        }
    }
}
