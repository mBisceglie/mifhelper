﻿using MifHelper.Common;
using MifHelper.Parser;
using MifHelper.Tokenizer;
using System.Collections.Generic;
using System.Globalization;

namespace MifHelper.Tags
{
    public class TextRectIDTag : IMifTag
    {
        public string TagName => "TextRectID";
        public MifComment? TagEndComment { get; set; }
        public int Id { get; set; }

        public TextRectIDTag(int id) => Id = id;

        public TextRectIDTag(List<MifToken> parameters, List<IMifNode> childs)
        {
            this.ValidateParameters(parameters, MifTokenType.Number);
            this.ValidateChilds(childs, false);
            Id = int.Parse(parameters[0].Value, CultureInfo.InvariantCulture);
        }

        public override string ToString() => $"<{TagName} {Id}>";

        public IEnumerable<MifToken> AsTokens(int level)
        {
            foreach (var token in this.GetTagStartAsTokens(level)) { yield return token; };
            yield return MifTokenFactory.WhiteSpaceToken;
            yield return MifTokenFactory.FromNumber(Id);
            foreach (var token in this.GetTagEndAsTokens(level)) { yield return token; };
        }
    }
}
