﻿using MifHelper.Common;
using MifHelper.Parser;
using MifHelper.Tokenizer;
using System.Collections.Generic;

namespace MifHelper.Tags
{
    public class ImportObFileDITag : IMifTag
    {
        public string TagName => "ImportObFileDI";
        public MifComment? TagEndComment { get; set; }
        public string Path { get; set; }

        public ImportObFileDITag(string path) => Path = path;

        public ImportObFileDITag(List<MifToken> parameters, List<IMifNode> childs)
        {
            this.ValidateParameters(parameters, MifTokenType.String);
            this.ValidateChilds(childs, false);
            Path = MifPathHelper.DecodePath(parameters[0].Value.TrimStart('`').TrimEnd('\''));
        }

        public override string ToString() => $"<{TagName} `{MifPathHelper.EncodePath(Path)}'>";

        public IEnumerable<MifToken> AsTokens(int level)
        {
            foreach (var token in this.GetTagStartAsTokens(level)) { yield return token; };
            yield return MifTokenFactory.WhiteSpaceToken;
            yield return MifTokenFactory.FromString(MifPathHelper.EncodePath(Path));
            foreach (var token in this.GetTagEndAsTokens(level)) { yield return token; };
        }
    }
}
