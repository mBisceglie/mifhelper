﻿using MifHelper.Common;
using MifHelper.Parser;
using MifHelper.Tokenizer;
using System.Collections.Generic;

namespace MifHelper.Tags
{
    public class FTagTag : IMifTag
    {
        public string TagName => "FTag";
        public MifComment? TagEndComment { get; set; }
        public string Font { get; set; }

        public FTagTag(string font) => Font = font;

        public FTagTag(List<MifToken> parameters, List<IMifNode> childs)
        {
            this.ValidateParameters(parameters, MifTokenType.String);
            this.ValidateChilds(childs, false);
            Font = MifStringHelper.DecodeString(parameters[0].Value.TrimStart('`').TrimEnd('\''));
        }

        public override string ToString() => $"<{TagName} `{MifStringHelper.EncodeString(Font)}'>";

        public IEnumerable<MifToken> AsTokens(int level)
        {
            foreach (var token in this.GetTagStartAsTokens(level)) { yield return token; };
            yield return MifTokenFactory.WhiteSpaceToken;
            yield return MifTokenFactory.FromString(MifStringHelper.EncodeString(Font));
            foreach (var token in this.GetTagEndAsTokens(level)) { yield return token; };
        }
    }
}
