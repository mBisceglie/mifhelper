﻿using MifHelper.Common;
using MifHelper.Parser;
using MifHelper.Tokenizer;
using System.Collections.Generic;
using System.Globalization;

namespace MifHelper.Tags
{
    public class FStretchTag : IMifTag
    {
        public string TagName => "FStretch";
        public MifComment? TagEndComment { get; set; }
        public decimal Value { get; set; }

        public FStretchTag(List<MifToken> parameters, List<IMifNode> childs)
        {
            this.ValidateParameters(parameters, MifTokenType.DecimalNumber, MifTokenType.Identifier);
            this.ValidateChilds(childs, false);
            Value = decimal.Parse(parameters[0].Value, CultureInfo.InvariantCulture);
        }

        public FStretchTag(string value) : this(decimal.Parse(value, CultureInfo.InvariantCulture)) { }

        public FStretchTag(decimal value)
        {
            Value = value;
        }

        public override string ToString() => $"<{TagName} {Value}%>";

        public IEnumerable<MifToken> AsTokens(int level)
        {
            foreach (var token in this.GetTagStartAsTokens(level)) { yield return token; };
            yield return MifTokenFactory.WhiteSpaceToken;
            yield return MifTokenFactory.FromDecimalNumber(Value);
            yield return MifTokenFactory.FromIdentifier("%");
            foreach (var token in this.GetTagEndAsTokens(level)) { yield return token; };
        }
    }
}
