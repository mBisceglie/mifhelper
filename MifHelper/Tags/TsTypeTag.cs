﻿using MifHelper.Common;
using MifHelper.Parser;
using MifHelper.Tokenizer;
using System;
using System.Collections.Generic;

namespace MifHelper.Tags
{
    public class TsTypeTag : IMifTag
    {
        public string TagName => "TSType";
        public MifComment? TagEndComment { get; set; }
        public string Value { get; set; }

        public TsTypeTag(string value) => Value = value;

        public TsTypeTag(List<MifToken> parameters, List<IMifNode> childs)
        {
            this.ValidateParameters(parameters, MifTokenType.Identifier);
            this.ValidateChilds(childs, false);
            Value = parameters[0].Value.ToLower() switch
            {
                "left" => "Left",
                "center" => "Center",
                "right" => "Right",
                "decimal" => "Decimal",
                _ => throw new AggregateException($"The '{TagName}' tag requires a alignment ('LeftRight', 'Left', 'Center' or 'Right') as parameter!")
            };
        }

        public override string ToString() => $"<{TagName} {Value}>";

        public IEnumerable<MifToken> AsTokens(int level)
        {
            foreach (var token in this.GetTagStartAsTokens(level)) { yield return token; };
            yield return MifTokenFactory.WhiteSpaceToken;
            yield return MifTokenFactory.FromIdentifier(Value);
            foreach (var token in this.GetTagEndAsTokens(level)) { yield return token; };
        }
    }
}