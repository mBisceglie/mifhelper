﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MifHelper.Tokenizer
{
    /// <summary>
    /// Tokenizer for the MIF format.
    /// </summary>
    public static class MifTokenizer
    {
        private enum TokenizerAction { IgnoreNextChar, Start, Read, FinishInclusive, FinishExclusive, Throw }

        /// <summary>
        /// Converts a sequence of tokens to a string.
        /// </summary>
        /// <param name="tokens">The sequence of tokens</param>
        /// <returns>The resulting string</returns>
        public static string ConvertTokensToString(IEnumerable<MifToken> tokens)
            => tokens.Aggregate(new StringBuilder(), (s, t) => s.Append(t.Value)).ToString();

        /// <summary>
        /// Converts a string into a sequence of tokens.
        /// </summary>
        /// <param name="data">The string to convert</param>
        /// <returns>The resulting list of tokens</returns>
        public static List<MifToken> Tokenize(string data) => Tokenize(data.AsSpan());

        /// <summary>
        /// Converts a span into a sequence of tokens.
        /// </summary>
        /// <param name="data">The span to convert</param>
        /// <returns>The resulting list of tokens</returns>
        public static List<MifToken> Tokenize(ReadOnlySpan<char> span)
        {
            var tokens = new List<MifToken>();
            var currentTokenType = MifTokenType.None;
            int i, startPos = 0, len = span.Length;

            for (i = 0; i < len; i++)
            {
                var charToProcess = span[i];

                (TokenizerAction action, MifTokenType tokenType) = GetNextTokenActionAndType(currentTokenType, charToProcess);

                currentTokenType = tokenType;

                switch (action)
                {
                    case TokenizerAction.IgnoreNextChar:
                        i++;
                        break;
                    case TokenizerAction.Start:
                        break;
                    case TokenizerAction.Read:
                        break;
                    case TokenizerAction.FinishInclusive:
                        tokens.Add(MifTokenFactory.FromTypeAndValue(currentTokenType, span[startPos..(i + 1)]));
                        startPos = i + 1;
                        currentTokenType = MifTokenType.None;
                        break;
                    case TokenizerAction.FinishExclusive:
                        tokens.Add(MifTokenFactory.FromTypeAndValue(currentTokenType, span[startPos..i]));
                        startPos = i;
                        currentTokenType = MifTokenType.None;
                        i--;
                        break;
                    case TokenizerAction.Throw:
                        var msg = $"Can't parse token {currentTokenType} with char '{charToProcess}' at pos {startPos}!";
                        if (i <= startPos) { throw new FormatException(msg); }
                        var data = new string(span[startPos..(i + 1)]);
                        throw new FormatException($"{msg} (data: {data})");
                }

            }

            if (startPos != span.Length) { tokens.Add(MifTokenFactory.FromTypeAndValue(currentTokenType, span[startPos..i])); }

            return tokens;
        }

        private static (TokenizerAction action, MifTokenType tokenType) GetNextTokenActionAndType(MifTokenType currentTokenType, char nextChar)
        {
            return (currentTokenType, nextChar) switch
            {
                (MifTokenType.Comment, '\n') => (TokenizerAction.FinishExclusive, currentTokenType),
                (MifTokenType.Comment, '\r') => (TokenizerAction.FinishExclusive, currentTokenType),
                (MifTokenType.Comment, _) => (TokenizerAction.Read, currentTokenType),

                (MifTokenType.WhiteSpace, ' ') => (TokenizerAction.Read, currentTokenType),
                (MifTokenType.WhiteSpace, '\t') => (TokenizerAction.Read, currentTokenType),
                (MifTokenType.WhiteSpace, _) => (TokenizerAction.FinishExclusive, currentTokenType),

                (MifTokenType.String, '\\') => (TokenizerAction.IgnoreNextChar, currentTokenType),
                //(MifTokenType.String, '\t') => (Action.Throw, currentType), // Not in MIF string allowed (but existis in some MIF files)
                (MifTokenType.String, '`') => (TokenizerAction.Throw, currentTokenType), // Not in MIF string allowed
                (MifTokenType.String, '>') => (TokenizerAction.Throw, currentTokenType), // Not in MIF string allowed
                (MifTokenType.String, '\'') => (TokenizerAction.FinishInclusive, currentTokenType),
                (MifTokenType.String, _) => (TokenizerAction.Read, currentTokenType),

                (MifTokenType.Number, '.') => (TokenizerAction.Read, MifTokenType.DecimalNumber),
                (MifTokenType.Number, _) => (char.IsDigit(nextChar) ? TokenizerAction.Read : TokenizerAction.FinishExclusive, currentTokenType),

                (MifTokenType.DecimalNumber, _) => (char.IsDigit(nextChar) ? TokenizerAction.Read : TokenizerAction.FinishExclusive, currentTokenType),

                (MifTokenType.Identifier, '/') => (TokenizerAction.Read, MifTokenType.Path),
                (MifTokenType.Identifier, '.') => (TokenizerAction.Read, MifTokenType.Path),
                (MifTokenType.Identifier, _) => (char.IsLetterOrDigit(nextChar) ? TokenizerAction.Read : TokenizerAction.FinishExclusive, currentTokenType),

                (MifTokenType.Path, _) => (char.IsLetterOrDigit(nextChar) ? TokenizerAction.Read : TokenizerAction.FinishExclusive, currentTokenType),

                (MifTokenType.None, ',') => (TokenizerAction.FinishInclusive, MifTokenType.Comma),
                (MifTokenType.None, '-') => (TokenizerAction.Start, MifTokenType.Number),
                (MifTokenType.None, '%') => (TokenizerAction.FinishInclusive, MifTokenType.Identifier),
                (MifTokenType.None, '"') => (TokenizerAction.FinishInclusive, MifTokenType.Identifier),
                (MifTokenType.None, '#') => (TokenizerAction.Start, MifTokenType.Comment),
                (MifTokenType.None, '\n') => (TokenizerAction.FinishInclusive, MifTokenType.LineBreak),
                (MifTokenType.None, '\r') => (TokenizerAction.FinishInclusive, MifTokenType.LineBreak),
                (MifTokenType.None, ' ') => (TokenizerAction.Start, MifTokenType.WhiteSpace),
                (MifTokenType.None, '\t') => (TokenizerAction.Start, MifTokenType.WhiteSpace),
                (MifTokenType.None, '`') => (TokenizerAction.Start, MifTokenType.String),
                (MifTokenType.None, '<') => (TokenizerAction.FinishInclusive, MifTokenType.TagStart),
                (MifTokenType.None, '>') => (TokenizerAction.FinishInclusive, MifTokenType.TagEnd),
                (MifTokenType.None, '(') => (TokenizerAction.FinishInclusive, MifTokenType.BracketStart),
                (MifTokenType.None, ')') => (TokenizerAction.FinishInclusive, MifTokenType.BracketEnd),
                (MifTokenType.None, '/') => (TokenizerAction.Start, MifTokenType.Path),
                (MifTokenType.None, _) => char.IsLetterOrDigit(nextChar)
                                            ? (TokenizerAction.Start, char.IsDigit(nextChar) ? MifTokenType.Number : MifTokenType.Identifier)
                                            : (TokenizerAction.Throw, currentTokenType),

                (_, _) => throw new Exception($"Invalid state: TokenizerFunction does not define rules for the current input! ({currentTokenType}, {nextChar})"),
            };
        }

    }
}
