﻿using System;

namespace MifHelper.Tokenizer
{

    /// <summary>
    /// Base class for tokens in the MIF. 
    /// </summary>
    public record MifToken
    {
        /// <summary>
        /// Type of the token.
        /// </summary>
        public MifTokenType Type { get; }

        /// <summary>
        /// The value of the token
        /// </summary>
        public string Value { get; }

        /// <summary>
        /// Creates a new token with the specified type and value.
        /// </summary>
        /// <param name="type">Type of the token.</param>
        /// <param name="value">Value of the token.</param>
        public MifToken(MifTokenType type, string value)
        {
            Type = type;
            Value = value;
        }

        /// <summary>
        /// Creates a new token from a span.
        /// </summary>
        /// <param name="type">The type of the token.</param>
        /// <param name="span">The span instance.</param>
        public MifToken(MifTokenType type, ReadOnlySpan<char> span)
          : this(type, new string(span)) { }

        public override string ToString() => $"{Type}: {Value}";

    }
}
