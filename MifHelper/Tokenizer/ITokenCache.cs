﻿namespace MifHelper.Tokenizer
{
    public interface ITokenCache
    {
        public MifToken FromTypeAndValue(MifTokenType mifTokenType, string value);
    }
}
