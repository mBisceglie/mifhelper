﻿using System.Collections.Generic;

namespace MifHelper.Tokenizer
{
    public class TokenCache : ITokenCache
    {
        private readonly Dictionary<string, MifToken> whiteSpaceTokenCache = new();
        private readonly Dictionary<string, MifToken> lineBreakTokenCache = new();
        private readonly Dictionary<string, MifToken> identifierTokenCache = new();

        public MifToken FromTypeAndValue(MifTokenType mifTokenType, string value)
        {
            if (value.Length > 10) { return new(mifTokenType, new string(value)); }
            return mifTokenType switch
            {
                MifTokenType.WhiteSpace => whiteSpaceTokenCache.TryGetValue(value, out var token)
                                            ? token
                                            : whiteSpaceTokenCache[value] = new(mifTokenType, value),
                MifTokenType.LineBreak => lineBreakTokenCache.TryGetValue(value, out var token)
                                            ? token
                                            : lineBreakTokenCache[value] = new(mifTokenType, value),
                MifTokenType.Identifier => identifierTokenCache.TryGetValue(value, out var token)
                                            ? token
                                            : identifierTokenCache[value] = new(mifTokenType, value),
                _ => new(mifTokenType, new string(value))
            };
        }

    }
}
