﻿using System;
using System.Collections.Generic;

namespace MifHelper.Tokenizer
{
    /// <summary>
    /// Utility class that defines extension methods for working with tokens.
    /// </summary>
    public static class MifTokenListExtensions
    {
        /// <summary>
        /// Returns the next non white space token.
        /// </summary>
        /// <param name="tokens">The token list</param>
        /// <param name="pos">Starting position</param>
        /// <returns>The next token</returns>
        public static MifToken NextToken(this IList<MifToken> tokens, ref int pos)
        {
            for (++pos; pos < tokens.Count; pos++)
            {
                if (tokens[pos].Type >= MifTokenType.Comment)
                {
                    return tokens[pos];
                }
            }
            throw new Exception($"Syntax error (at pos {pos}): " +
                $"The end of the token stream was reached before the next token was found!");
        }
    }
}
