﻿using MifHelper.Parser;
using MifHelper.Tokenizer;
using System;
using System.Collections.Generic;

namespace MifHelper.Common
{
    public static class TagValidationExtensions
    {
        public static void ValidateParameters(this IMifTag tag, IList<MifToken> parameters, int minParameters, int? maxParameters = null)
        {
            maxParameters ??= minParameters;
            if (parameters.Count < minParameters || parameters.Count > maxParameters)
            {
                if (maxParameters == 0)
                {
                    throw new ArgumentException($"The '{tag.TagName}' tag does not allow parameters!");
                }
                else if (minParameters == 1)
                {
                    throw new ArgumentException($"The '{tag.TagName}' tag requires a parameter!");
                }
                else
                {
                    throw new ArgumentException($"The '{tag.TagName}' tag requires {minParameters} parameters!");
                }
            }
        }

        public static void ValidateParameters(this IMifTag tag, IList<MifToken> parameters, params MifTokenType[] parameterDefinitions)
        {
            ValidateParameters(tag, parameters, parameterDefinitions.Length);
            int i = 0;
            foreach (var parameterType in parameterDefinitions)
            {
                if (parameterType != MifTokenType.None &&
                    (parameters[i].Type != parameterType
                    && (parameterType != MifTokenType.DecimalNumber || parameters[i].Type != MifTokenType.Number)))
                {
                    throw new ArgumentException($"The {i + 1}. parameter of the '{tag.TagName}' tag must be of type '{parameterType}'!");
                }
                i++;
            }
        }

        public static void ValidateValueWithUnitParameters(this IMifTag tag, IList<MifToken> parameters, int count)
        {
            MifTokenType[] parameterDefinitions = new MifTokenType[count * 2];
            for (int i = 0; i < count; i++)
            {
                parameterDefinitions[i * 2] = MifTokenType.DecimalNumber;
                parameterDefinitions[i * 2 + 1] = MifTokenType.Identifier;
            }
            ValidateParameters(tag, parameters, parameterDefinitions);
        }

        public static void ValidateChilds(this IMifTag tag, IList<IMifNode> childs, bool allowChilds)
        {
            if (childs.Count > 0 && !allowChilds) { throw new ArgumentException($"The '{tag.TagName}' Tag does not support childs elements!"); }
        }

    }
}
