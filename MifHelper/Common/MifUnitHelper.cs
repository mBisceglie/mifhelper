﻿using MifHelper.Enums;
using System;

namespace MifHelper.Common
{
    public static class MifUnitHelper
    {
        public static MifUnit StringToMifUnit(string unit)
        {
            return unit.ToLower() switch
            {
                "pt" => MifUnit.Point,
                "point" => MifUnit.Point,
                "\"" => MifUnit.Inch,
                "in" => MifUnit.Inch,
                "mm" => MifUnit.Millimeter,
                "millimeter" => MifUnit.Millimeter,
                "cm" => MifUnit.Centimeter,
                "centimeter" => MifUnit.Centimeter,
                "pc" => MifUnit.Pica,
                "pica" => MifUnit.Pica,
                "dd" => MifUnit.Didot,
                "didot" => MifUnit.Didot,
                "cc" => MifUnit.Cicero,
                "cicero" => MifUnit.Cicero,
                "px" => MifUnit.Pixel,
                _ => throw new ArgumentException($"Unit '{unit.ToLower()}' is not supported.", nameof(unit))
            };
        }

        public static string MifUnitToString(MifUnit mifUnit)
        {
            return mifUnit switch
            {
                MifUnit.Point => "pt",
                MifUnit.Inch => "in",
                MifUnit.Millimeter => "mm",
                MifUnit.Centimeter => "cm",
                MifUnit.Pica => "pc",
                MifUnit.Didot => "dd",
                MifUnit.Cicero => "cc",
                MifUnit.Pixel => "px",
                _ => throw new ArgumentException($"MifUnit '{mifUnit}' is not supported.", nameof(mifUnit))
            };
        }
    }
}
